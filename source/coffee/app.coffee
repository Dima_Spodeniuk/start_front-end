class App
	constructor: () ->
		@initFormValidation()
		@initInputMask()
		if $.browser.desktop then @initScrollAnimation() else null
		@handleFormSubmit()
		if $.browser.desktop then @handleScrollAnimation() else null

	initInputMask: () ->
		$(".tel-mask").mask "+38(999)-999-99-99"

	initFormValidation: () ->
		$('.contacts-form').each (index) ->
			$(this).validate
				errorPlacement: () -> return false

				submitHandler: (form) ->
					$(form).trigger('form.valid')
					return false

	initScrollAnimation: () ->
		$.fn.animated = (animation, offset) ->
			$(this).css('opacity', '0').waypoint ((dir) ->
				if dir == 'down'
					$(this.element).addClass(animation)
			), offset: offset
		console.log 'Scroll animation init.'

	handleFormSubmit: () ->
		$('.form-submit').on 'form.valid', () ->

	handleScrollAnimation: () ->
		$(".fade-in, .rotate-in, .zoom-in, .fade-in-up").animated "animated", "90%"