Start project
```sh
bower install
npm install
```
Sprites ( generate from folder (./img/sprite-source) )
```sh
gulp sprite-png 
gulp sprite-svg
```
Svg to font ( generate from folder (./img/sprite-source) )
```sh
gulp svg-font
```