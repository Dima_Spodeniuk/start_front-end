var gulp = require('gulp'),
	sass = require('gulp-sass'),
	errorNotifier = require('gulp-error-notifier'),
	spritesmith = require('gulp.spritesmith'),
	iconfont = require('gulp-iconfont'),
	coffee = require('gulp-coffee'),
	iconfontCss = require('gulp-iconfont-css'),
	svgstore = require('gulp-svgstore'),
	svgmin = require('gulp-svgmin'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	jade = require('gulp-jade'),
	extractSourcemap = require('gulp-extract-sourcemap'),
	sourcemaps = require('gulp-sourcemaps'),
	uglify = require('gulp-uglify'),
	domSrc = require('gulp-dom-src'),
	browserSync = require('browser-sync').create();

var fontName = 'Icons-font',
	jsMinifyFrom = './index.html';
	minifyCssName = 'app.min.css'

gulp.task('serve', function() {
	browserSync.init({
		server: './'
	});
});

gulp.task('scss', function() {
	gulp.src('./source/scss/*.scss')
		.pipe(errorNotifier())
		.pipe(sourcemaps.init())
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.pipe(sourcemaps.write())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('./css'));
});

gulp.task('jade', function() {
	gulp.src('./source/jade/*.jade')
		.pipe(jade({
			pretty: '	'
		}))
		.pipe(gulp.dest('./'));
});

gulp.task('coffee', function() {
	gulp.src('./source/coffee/*.coffee')
		.pipe(errorNotifier())
		.pipe(coffee({
			bare: true
		}))
		.pipe(gulp.dest('js'));
});

gulp.task('minify-js', function() {
	domSrc({ file: jsMinifyFrom, selector: 'script', attribute: 'src' })
		.pipe(errorNotifier())
		.pipe(concat('app.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./js/dist'));
});

gulp.task('remove-sourcemap', function() {
	gulp.src('./source/scss/*.scss')
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('./css'));
});

gulp.task('front-end', ['jade', 'scss', 'coffee', 'serve'], function() {
	gulp.watch('./source/jade/**/*.jade', ['jade']);
	gulp.watch('./source/coffee/*.coffee', ['coffee']);
	gulp.watch('./source/scss/**/*.scss', ['scss']);

	gulp.watch("./*.html").on('change', browserSync.reload);
	gulp.watch("./css/*.css").on('change', browserSync.reload);
	gulp.watch("./js/*.js").on('change', browserSync.reload);
});

gulp.task('wordpress', ['scss', 'coffee'], function() {
	gulp.watch('./source/coffee/*.coffee', ['coffee']);
	gulp.watch('./source/scss/**/*.scss', ['scss']);
});

gulp.task('build', ['minify-js', 'remove-sourcemap']);

gulp.task('sprite-png', function() {
	gulp.src('./img/sprite-source/*.png')
		.pipe(spritesmith({
			imgName: 'sprite.png',
			cssName: '../source/scss/modules/_sprite.scss',
			padding: 1,
			imgPath: '../img/sprite.png',
		}))
		.pipe(errorNotifier())
		.pipe(gulp.dest('img'));
});

gulp.task('sprite-svg', function() {
	gulp.src('./img/sprite-source/*.svg')
		.pipe(svgmin())
		.pipe(svgstore())
		.pipe(rename({
			basename: "svg-sprite.min"
		}))
		.pipe(gulp.dest('./source'));
});

gulp.task('svg-font', function() {
	gulp.src(['./img/sprite-source/*.svg'])
		.pipe(iconfontCss({
			fontName: fontName,
			path: 'scss',
			targetPath: '../../source/scss/modules/_sprite.scss',
			fontPath: '../fonts/icons/',
			cssClass: 'icon',
			centerHorizontally: true
		}))
		.pipe(iconfont({
			fontName: fontName,
			prependUnicode: true,
			formats: [
				'ttf',
				'eot',
				'woff',
				'woff2',
				'svg'
			],
		}))
		.pipe(errorNotifier())
		.pipe(gulp.dest('./fonts/icons'));
});